package com.sjinc.service;

import com.sjinc.model.전송결과;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Component
public class NackService implements Processor {

  @Override
  public void process(Exchange exchange) throws Exception {

    전송결과 result = new 전송결과();

    result.set상태("FAIL : " + exchange.getException().getMessage());

    Message in = exchange.getIn();
    in.setHeader(Exchange.CONTENT_TYPE, "application/json");
    in.setBody(result);

  }
}
