package com.sjinc.service;

import com.sjinc.model.HealthResult;
import org.springframework.stereotype.Component;

@Component
public class HealthService {

  public HealthResult process() {

    HealthResult result = new HealthResult();

    result.setStatus("OK");

    return result;
  }
}
