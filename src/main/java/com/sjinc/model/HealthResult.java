package com.sjinc.model;

public class HealthResult {
  String status;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}
